"""Module for functions related to finding route. Function dijkstra used by site."""
import networkx as nx


LARGE = 1e3


class SolutionNotFound(Exception):
    pass


def dijkstra(graph, stop_from, stop_to):
    """Dijkstra's algorithm for finding shortest path."""
    
    unvisited = {stop for stop in graph.nodes}
    
    # initialize distances to a sufficiently large number
    nx.set_node_attributes(graph, LARGE, 'distance')
    
    # start from starting stop
    graph.nodes[stop_from]['distance'] = 0
    current = stop_from

    while True:
        # all neighbours of current stop which are not visited
        unvisited_neighbours = [(d['weight'], n) for n, d in graph[current].items() if n in unvisited]
        
        # if distance of neighbour through current stop shorter than previously, update
        for tentative_distance, neighbour in unvisited_neighbours:
            tentative_distance += graph.nodes[current]['distance']
            current_distance = graph.nodes[neighbour]['distance']
            if tentative_distance < current_distance:
                graph.nodes[neighbour]['distance'] = tentative_distance
        
        # no nodes left to search, solution not found
        if min(graph.nodes[n]['distance'] for n in unvisited) == LARGE:
            raise SolutionNotFound

        # set current as visited, find from unvisited stop with shortest tentative distance
        unvisited.remove(current)
        possible_next = sorted((graph.nodes[n]['distance'], n) for n in unvisited)

        # solution found if destination has been visited
        if stop_to not in unvisited or not possible_next:
            break

        _, current = possible_next[0]

    # find path through shortest edges
    path = []
    end = stop_to
    while end != stop_from:
        start = end
        possible = [(n, graph.edges[start, n]['weight'] + graph.nodes[n]['distance']) for n in graph[start]]
        end, _ = sorted(possible, key=lambda x: x[1])[0]
        # TODO change bus route only if required
        path.append(((end, start), graph.edges[start, end]['routes'][0]))

    return list(reversed(path))


def astar(graph, stop_from, stop_to):
    """A* route finding using networkx builtin function. For testing."""
    try:
        path = nx.algorithms.shortest_paths.astar_path(graph, stop_from, stop_to)
    except nx.NetworkXNoPath:
        raise SolutionNotFound
    # TODO change bus route only if required
    return [(e, graph.get_edge_data(*e)['routes'][0]) for e in zip(path, path[1:])]


def cost(graph, path):
    """Cost (length) of taken path on graph."""
    return sum(graph.edges[e]['weight'] for e, _ in path)
