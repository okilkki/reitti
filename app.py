"""Module for launching site using Flask."""
from flask import Flask, request
from main import display_site


app = Flask(__name__)


@app.route('/')
def hello():
    return display_site(request)
