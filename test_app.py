import pytest
import flask
import itertools

from main import display_site, load_graph
from routing import dijkstra, astar, cost, SolutionNotFound


app = flask.Flask(__name__)

graph = load_graph()
stops = graph.nodes
test_routes = list(itertools.permutations(stops, 2))


@pytest.mark.parametrize('args', ['', '?to=A'])
def test_site_empty(args):
    with app.test_request_context(args):
        assert 'Find route' in str(display_site(flask.request).data)


def test_site_route():
    with app.test_request_context('/?from=K&to=I'):
        assert 'Route' in str(display_site(flask.request).data)


@pytest.mark.parametrize('start,end', test_routes)
def test_route(start, end):
    try:
        path_astar = astar(graph, start, end)
    except SolutionNotFound:
        with pytest.raises(SolutionNotFound):
            dijkstra(graph, start, end)
    
    path_dijkstra = dijkstra(graph, start, end)

    assert cost(graph, path_astar) == cost(graph, path_dijkstra)
