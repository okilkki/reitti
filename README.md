# Reittiopas

Minimal solution for searching route for Solidabis [code challenge](https://koodihaaste.solidabis.com/).

Made on Linux (& a bit on Windows) as a simple Flask solution with a landing page and results (and error page) under the same endpoint.

See [running version](https://us-central1-route-ok.cloudfunctions.net/route) as Google Cloud Function.



## Getting started
```
> git clone https://gitlab.com/okilkki/reitti.git
> cd reitti
> python -m pip install -r requirements.txt
```

Linux:

```
> export FLASK_APP="app"
> flask run
```

Windows:

```
> $env:FLASK_APP = "app"
> python -m flask run
```


### Build with

* [flask](https://flask.palletsprojects.com/en/1.1.x/)
* [networkx](https://networkx.github.io/)
* [xkcd-font](https://github.com/ipython/xkcd-font)
* [pytest](https://docs.pytest.org/en/latest/) - tests


## TODO

* Actual frontend


## FAQ

* Why Google Cloud Function? Wanted to try, which is why the solution is so simplistic.
