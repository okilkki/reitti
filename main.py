"""Module for displaying site pages. Launched by app.py and/or Google Cloud Function."""
import json
import networkx as nx
from flask import render_template
from flask_cors import cross_origin

import routing


@cross_origin()
def display_site(request):
    """Display landing page if stop arguments not defined, otherwise try to find route between stops."""
    graph = load_graph()

    try:
        stop_from = request.args['from']
        stop_to = request.args['to']
    except KeyError:
        return start_page(graph.nodes)

    if not (stop_from in graph.nodes and stop_to in graph.nodes):
        return error_page("Stops not found")

    try:
        path = routing.dijkstra(graph, stop_from, stop_to)
        return result_page(path)
    except routing.SolutionNotFound:
        return error_page("Solution not found")


def start_page(stops):
    return render_template('landing.html', stops=stops)


def error_page(e):
    return f"Error: {e}"


def result_page(path):
    return render_template('results.html', path=path)


def load_graph():
    """Load and initialize graph from local file."""
    with open('reittiopas.json', encoding="utf-8") as routes_file:
        data = json.load(routes_file)

    roads = data['tiet']
    routes = data['linjastot']

    road_graph = nx.Graph()
    for road in roads:
        road_graph.add_edge(road['mista'], road['mihin'], weight=road['kesto'])

    route_graph = nx.Graph()
    for route_name, route_stops in routes.items():
        for stop_from, stop_to in zip(route_stops, route_stops[1:]):
            weight = road_graph.edges[stop_from, stop_to]['weight']
            if route_graph.has_edge(stop_from, stop_to):
                route_graph[stop_from][stop_to]['routes'].append(route_name)
            else:
                route_graph.add_edge(stop_from, stop_to, weight=weight, routes=[route_name])

    return route_graph
